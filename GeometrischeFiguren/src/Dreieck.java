import java.awt.*;

public class Dreieck extends Figur{

    private double seiteA;
    private double seiteB;
    private double seiteC;

    public Dreieck(double seiteA, double seiteB, double seiteC, Color farbe){
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.seiteC = seiteC;
        this.farbe = farbe;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    public double getSeiteC() {
        return seiteC;
    }

    public void setSeiteC(double seiteC) {
        this.seiteC = seiteC;
    }

    public double alpha(){
        double winkel;
        winkel = Math.acos((Math.pow(this.seiteC, 2) + Math.pow(this.seiteB, 2) - Math.pow(seiteA, 2))/(2*this.seiteB*this.seiteC));
        double winkel2 = (winkel/Math.PI) * 180;
        return winkel2;
    }

    public double beta(){
        double winkel;
        winkel = Math.acos((Math.pow(this.seiteA, 2) + Math.pow(this.seiteC, 2) - Math.pow(this.seiteB, 2))/(2 * this.seiteA * this.seiteC));
        double winkel2 = (winkel/Math.PI) * 180;
        return winkel2;
    }
    public double gamma(){
        double winkel;
        winkel =Math.acos((Math.pow(this.seiteA, 2) + Math.pow(this.seiteB, 2) - Math.pow(this.seiteC, 2))/(2 * this.seiteA * this.seiteB));
        double winkel2 = (winkel/Math.PI) * 180;
        return winkel2;
    }
    public double hoeheA(){
        double hoehe = this.seiteC * Math.sin((beta()/180) * Math.PI);
        return hoehe;
    }
    public double hoeheB(){
        double hoehe = this.seiteC * Math.sin((alpha()/180) * Math.PI);
        return hoehe;
    }
    public double hoeheC(){
        double hoehe = this.seiteA * Math.sin((beta()/180) * Math.PI);
        return hoehe;
    }

    @Override
    public double umfang() {
        return (this.seiteA + this.seiteB + this.seiteC);
    }

    @Override
    public double flaeche() {
        return ((this.seiteC * hoeheC())/2);
    }

    @Override
    public String toString() {
        return "Dreieck{" +
                "seiteA=" + seiteA +
                ", seiteB=" + seiteB +
                ", seiteC=" + seiteC +
                ", farbe=" + farbe +
                '}';
    }
}
