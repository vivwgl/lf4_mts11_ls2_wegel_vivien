import java.awt.*;

abstract class Figur {

    protected Color farbe = Color.BLUE;

    public Color getFarbe() {
        return farbe;
    }

    public void setFarbe(Color farbe) {
        this.farbe = farbe;
    }

    public double umfang() {
        return 0;
    }

    public double flaeche(){

        return 0;
    }

    public String toString(){

        return null;
    }
}
