import java.awt.*;

public class Kreis extends Figur{

    private double radius;

    public Kreis(double radius, Color farbe) {
        this.radius = radius;
        this.farbe = farbe;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double flaeche() {
        return (Math.PI * Math.pow(this.radius, 2));
    }

    @Override
    public double umfang() {
        return (2 * this.radius * Math.PI);
    }

    @Override
    public String toString() {
        return "Kreis{" +
                "farbe=" + farbe +
                ", radius=" + radius +
                '}';
    }
}
