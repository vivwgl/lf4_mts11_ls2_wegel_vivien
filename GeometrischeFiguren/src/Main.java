import java.awt.*;

public class Main {
    public static void main(String[] args){
       /* Dreieck dreieck1 = new Dreieck(3, 4, 4, Color.BLUE);
        System.out.println("Alpha: " + dreieck1.alpha());
        System.out.println("Beta: " +dreieck1.beta());
        System.out.println("Gamma: " +dreieck1.gamma());
        System.out.println("HöheA: " +dreieck1.hoeheA());
        System.out.println("HöheB: " +dreieck1.hoeheB());
        System.out.println("HöheC: " +dreieck1.hoeheC());
        System.out.println("Unfang: " +dreieck1.umfang());
        System.out.println("Fläche: " +dreieck1.flaeche());
        System.out.println(dreieck1); */

        /*Rechteck rechteck1 = new Rechteck(5, 4, Color.BLUE);
        System.out.println("Umfang: " + rechteck1.umfang());
        System.out.println("Fläche: " + rechteck1.flaeche());
        System.out.println(rechteck1);*/

        /*Kreis kreis1 = new Kreis(5, Color.WHITE);
        System.out.println("Umfang: " + kreis1.umfang());
        System.out.println("Fläche: " + kreis1.flaeche());
        System.out.println(kreis1);*/

        Parallelogramm parallelogramm1 = new Parallelogramm(4, 3, 36, Color.BLACK);
        System.out.println(parallelogramm1.flaeche());
        System.out.println(parallelogramm1.umfang());
        System.out.println(parallelogramm1.getDiagonalen());
        System.out.println(parallelogramm1.getBeta());
        System.out.println(parallelogramm1);

    }
}
