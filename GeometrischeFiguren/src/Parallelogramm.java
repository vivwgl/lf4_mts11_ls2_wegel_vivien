import java.awt.*;

public class Parallelogramm extends Figur{

    private double seiteA;
    private double seiteB;
    private double winkelA;
    private double beta ;
    private double diagonalen ;

    public Parallelogramm(double seiteA, double seiteB, double alpha, Color farbe) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.winkelA = alpha;
        this.farbe = farbe;
        this.beta = berechneBeta(alpha);
        this.diagonalen = berechneDiagonale(this.beta);
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    public double getWinkelA() {
        return winkelA;
    }

    public void setWinkelA(double winkel) {
        this.winkelA = winkel;
    }

    public double getDiagonalen() {
        return diagonalen;
    }

    public void setDiagonalen(double diagonalen) {
        this.diagonalen = diagonalen;
    }

    public double berechneBeta(double alpha){
        double beta = (360 - (2*alpha))/2;
        return beta;
    }



    public double berechneDiagonale(double beta){
        double diagonale =Math.sqrt(Math.pow(this.seiteA, 2) + Math.pow(this.seiteB, 2) - 2 * this.seiteA * this.seiteB * Math.cos(beta));
        return diagonale;
    }

    public double hoeheA(){
        double hoehe = this.diagonalen * Math.sin((this.winkelA/180) * Math.PI);
        return hoehe;
    }
    @Override
    public double umfang() {
        return (2 * (seiteA + seiteB));
    }

    @Override
    public double flaeche() {
        return (this.seiteA * hoeheA());
    }

    @Override
    public String toString() {
        return "Parallelogramm{" +
                "farbe=" + farbe +
                ", seiteA=" + seiteA +
                ", seiteB=" + seiteB +
                ", winkel=" + winkelA +
                ", diagonalen=" + diagonalen +
                '}';
    }
}
