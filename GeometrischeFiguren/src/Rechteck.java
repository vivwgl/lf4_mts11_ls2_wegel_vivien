import java.awt.*;

public class Rechteck extends Figur{

    private double seiteA;
    private double seiteB;

    public Rechteck(double seiteA, double seiteB, Color farbe) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    @Override
    public double flaeche() {
        return (this.seiteA * this.seiteB);
    }

    @Override
    public double umfang() {
        return (2 * (this.seiteA + this.seiteB));
    }

    @Override
    public String toString() {
        return "Rechteck{" +
                "farbe=" + farbe +
                ", seiteA=" + seiteA +
                ", seiteB=" + seiteB +
                '}';
    }
}
