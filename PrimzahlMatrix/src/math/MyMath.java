package math;

public class MyMath {
	
	public static boolean isPrim(long zahl) {
		//boolean isPrimzahl = true;
		
		if(zahl < 2) {
			return false;
		}
		
		
		for(int i = 2; i < zahl; i++) {
			if(zahl % i == 0) {
				return false;
			}
		}
		
		return true;
	}
	
	public static long round_off(long number) {
		
		if(number < 10000) {
			return number = 0;
		} else {
			number = number - (number % 10000);
		}
		
		return number;
	}

}

