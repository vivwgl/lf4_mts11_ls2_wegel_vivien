package math;

import java.util.ArrayList;
import java.util.List;

public class PrimGenerator {
	
	
	public static boolean[] generatePrimNumbers(){
		
		return generatePrimNumbers(1);
		
	}
	
	public static boolean[] generatePrimNumbers(long startnumber){
		boolean[] primList = new boolean[10000];
		
		long iterable = startnumber + 10000;
		
		for(long i = startnumber; i < iterable; i++) {
			
			primList[(int) (i-startnumber)] = MyMath.isPrim(i);
			
		}
		
		return primList;
	}

}
