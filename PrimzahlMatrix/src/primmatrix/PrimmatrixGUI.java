package primmatrix;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import math.PrimGenerator;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.FlowLayout;

public class PrimmatrixGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrimmatrixGUI frame = new PrimmatrixGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrimmatrixGUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1500, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Enter startnumber: ");
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(50);
		//String s = textField.getText();
		//long startwert = Long.parseLong(s);
		
		JButton btnNewButton = new JButton("Submit");
		panel.add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 200, 0, 0));
		
		long startwert = 500;
		boolean[] zahlen = new boolean[10000];
		
		if( startwert <= 1) {
			zahlen = PrimGenerator.generatePrimNumbers(1);
		} else {
			zahlen = PrimGenerator.generatePrimNumbers(startwert);
		}
		
		long endwert = startwert + 10000 -1;
		
		setTitle(startwert + " to " + endwert);
		
		for(int i = 0; i < 10000; i++) {
			JPanel tmp = new JPanel();
			tmp.setSize(5, 5);
			tmp.setBorder(new LineBorder(new Color(200, 200, 200)));
			if(zahlen[i]) {
				tmp.setBackground(Color.RED);
			} else {
				tmp.setBackground(Color.WHITE);
			}
			String toolTip = (i + startwert) + "";
			tmp.setToolTipText(toolTip);
			panel_1.add(tmp);
		}
		
	}

}
