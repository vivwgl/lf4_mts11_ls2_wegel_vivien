package test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import math.MyMath;

public class MyMathTest {
	
	@Test 
	void test_eins() {
		boolean prim = MyMath.isPrim(1);
		assertEquals(prim, false);
	}
	
	@Test 
	void test_zwei() {
		boolean prim = MyMath.isPrim(2);
		assertEquals(prim, true);
	}
	
	@Test 
	void test_5011() {
		boolean prim = MyMath.isPrim(5011);
		assertEquals(prim, true);
	}
	
	@Test 
	void test_9973() {
		boolean prim = MyMath.isPrim(9973);
		assertEquals(prim, true);
	}
	
	@Test 
	void test_10000() {
		boolean prim = MyMath.isPrim(10000);
		assertEquals(prim, false);
	}

}
