package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import math.PrimGenerator;

public class PrimGeneratorTest {
	
	@Test
	void test_eins_bis_16(){
		boolean[] primzahen_array = {false, true, true, false, true, false, true, false, false, false, true, false, true, false, false, false}; 
		boolean[] primzahlen_generated = PrimGenerator.generatePrimNumbers();
		boolean[] prim_range = Arrays.copyOfRange(primzahlen_generated, 0, 16);
		assertEquals(true, Arrays.equals(primzahen_array, prim_range));
	}
	
	@Test
	void test_false(){
		boolean[] primzahen_array = {false, true, true, false, true, false, true, false, false, false, true, false, true, false, false, false}; 
		boolean[] primzahlen_generated = PrimGenerator.generatePrimNumbers();
		boolean[] prim_range = Arrays.copyOfRange(primzahlen_generated, 1, 17);
		assertEquals(false, Arrays.equals(primzahen_array, prim_range));
	}
	
	@Test
	void test_35_bis_50(){
		boolean[] primzahen_array = {false, false, true, false, false, false, true, false, true, false, false, false, true, false, false, false}; 
		boolean[] primzahlen_generated = PrimGenerator.generatePrimNumbers();
		boolean[] prim_range = Arrays.copyOfRange(primzahlen_generated, 34, 50);
		assertEquals(true, Arrays.equals(primzahen_array, prim_range));
	}
	
	@Test
	void test_9320_bis_9340(){
		boolean[] primzahen_array = {false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false}; 
		boolean[] primzahlen_generated = PrimGenerator.generatePrimNumbers();
		boolean[] prim_range = Arrays.copyOfRange(primzahlen_generated, 9319, 9340);
		assertEquals(true, Arrays.equals(primzahen_array, prim_range));
	}
	
	@Test
	void test_357_bis_370(){
		boolean[] primzahen_array = {false, false, true, false, false, false, false, false, false, false, true, false, false, false}; 
		boolean[] primzahlen_generated = PrimGenerator.generatePrimNumbers();
		boolean[] prim_range = Arrays.copyOfRange(primzahlen_generated, 356, 370);
		assertEquals(true, Arrays.equals(primzahen_array, prim_range));
	}

}
