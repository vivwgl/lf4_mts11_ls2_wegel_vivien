/**
 * <h1>Ladung </h1>
 * Die Klasse Ladung beinhaltet die Ladung eines Raumschiff und wird direkt dem jeweiligen Raumschiff zugeordnet.
 *
 * @author vwgl
 * @version 1.0
 * @since 2022-02-26
 */

public class Ladung {
    private String bezeichnung;
    private int menge;

    /**
     * Leerer Konstruktor der Klasse Ladung.
     */
    public Ladung() {

    }

    /**
     * Vollparametisierter Konstruktor der Klasse Ladung.
     * @param raumschiff Der Name ist das Raumschiff, welches der Ladung zugewiesen wird.
     * @param bezeichnung Die Bezeichnung ist der Name beziehungsweise der Typ der Ladung.
     * @param menge Die Menge beschreibt die Anzahl der jeweiligen Ladung.
     */
    public Ladung(Raumschiffe raumschiff, String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    /**
     * Setter f�r die Bezeichnung der Ladung.
     * @param typLadung Beinhaltet die Art/den Namen der Ladung.
     */
    public void setBezeichnung(String typLadung) {
        bezeichnung = typLadung;
    }

    /**
     * Getter f�r die Bezeichnung der Ladung.
     * @return Die Art/den Namen der Ladung als String.
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Setter f�r die Anzahl der Ladung.
     * @param anzahl Anzahl der Ladung von der jeweiligen Art.
     */
    public void setMenge(int anzahl) {
        menge = anzahl;
    }

    /**
     * Getter f�r die Anzahl der Ladung.
     * @return Die Anzahl der Ladung einer Art als int
     */
    public int getMenge() {
        return menge;
    }

    /**
     * Overide der tiStrin() Methode um die Attribute der Klasse Ladung ausgeben zu lassen, anstelle der Objektbezeichnung.
     * @return Die Attribute der Klasse Laudung.
     */
    @Override
    public String toString() {
        return "\tBezichnung: " + bezeichnung + ", Menge: " + menge;
    }
}
