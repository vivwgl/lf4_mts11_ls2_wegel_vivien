
import java.util.List;

/**
 * Die Klasse Main dient dazu, die Raumschiffe zu initialisieren und ihre Methoden aufzurufen. In dem Falle findet ein Kampf zwischen den Raumschiffen statt.
 * @author  vwgl
 * @version 1.0
 * @since 2022-02-26
 */
public class Main {
    /**
     * Main-Methode:
     * Erstellen von drei Distanzen, die gegeneinander K�mpfen/Nachrichten an den Broadcast-Kommunikator senden.
     * @param args
     */
    public static void main(String[] args) {

        List<String> bk = Raumschiffe.broadcastKommunikator;
        Ladung[] klingonenLadung = new Ladung[2];
        Raumschiffe klingonen = new Raumschiffe("IKS Hegh'ta", 100, 100, 100, 100, 1, 2, klingonenLadung, bk);
        Ladung schneckensaft = new Ladung(klingonen, "Ferengi Schneckensaft", 200);
        Ladung schwerter = new Ladung(klingonen, "Bat'leth Klingonen Schwert", 200);
        klingonenLadung[0] = schneckensaft;
        klingonenLadung[1] = schwerter;


        Ladung[] romulanerLadung = new Ladung[3];
        Raumschiffe romulaner = new Raumschiffe("IRW Khazara", 100, 100, 100, 100, 2, 2, romulanerLadung, bk);
        Ladung schrott = new Ladung(romulaner, "Borg-Schrott", 5);
        Ladung materie = new Ladung(romulaner, "Rote Materie", 2);
        Ladung waffe = new Ladung(romulaner, "Plasma-Waffe", 50);
        romulanerLadung[0] = schrott;
        romulanerLadung[1] = materie;
        romulanerLadung[2] = waffe;

        Ladung[] vulkanierLadung = new Ladung[2];
        Raumschiffe vulkanier = new Raumschiffe("Ni'Var", 80, 80, 100, 50, 0, 5, vulkanierLadung, bk);
        Ladung sonde = new Ladung(vulkanier, "Forschungssonde", 35);
        Ladung torpedo = new Ladung(vulkanier, "Photonentorpedo", 3);
        vulkanierLadung[0] = sonde;
        vulkanierLadung[1] = torpedo;

        klingonen.photonenAbschiessen(1, romulaner);
        romulaner.phaserkanonenAbschiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        klingonen.photonenAbschiessen(2, romulaner);
        klingonen.zustandAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        klingonen.zustandAusgeben();
        romulaner.ladungsverzeichnisAusgeben();
        romulaner.zustandAusgeben();
        vulkanier.ladungsverzeichnisAusgeben();
        vulkanier.zustandAusgeben();
        System.out.println(klingonen.logbuchEintraege());

    }
}
