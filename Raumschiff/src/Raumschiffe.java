import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Raumschiff</h1>
 * Die Klasse zum erstellen einer Instanz "Raumschiff" mit jeweiliger Ladung und einem von allen geteilten Broadcastkommunikator.
 * @author vwgl
 * @version 1.0
 * @since 2022-02-26
 */

public class Raumschiffe {
    /**
     * Die Klasse wird genutzt um ein Raumschiff zu erstellen.
     */
    private String name;
    private int energieversorgungInProzent;
    private int schutzschildeInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int huelleInProzent;
    private int photonentorpedos;
    private int reparaturAndroiden;
    private Ladung[] ladungsverzeichnis;
    public static List<String> broadcastKommunikator = new ArrayList<>();

    /**
     * Leerer Konstruktor der Klasse Raumschiff.
     */
    public Raumschiffe() {

    }

    /**
     * Vollparametisierter Konstruktor der Klasse Raumschiff.
     * @param name Raumschiffbezeichnung
     * @param energieversorgungInProzent Energieversorgung des Raumschiffs, Angabe in Prozent
     * @param schutzschildeInProzent Schutzschild des Raumschiffs, Angabe in Prozent
     * @param lebenserhaltungssystemeInProzent Lebenserhaltungssystem des Raumschiffs, Angabe in Prozent
     * @param huelleInProzent H�lle des Raumschiffs, Angabe in Prozent
     * @param photonentorpedos Anzahl der auf dem Raumschiff vorhandenen Photonentorpedos
     * @param repraturAndroiden Anzahl der Reparaturandroiden
     * @param ladungsverzeichnis Ladung auf dem Raumschiff
     * @param broadcastKommunikator Nachrichten die an alle Raumschiffe gehen
     */
    public Raumschiffe(String name, int energieversorgungInProzent, int schutzschildeInProzent, int lebenserhaltungssystemeInProzent, int huelleInProzent, int photonentorpedos, int repraturAndroiden, Ladung[] ladungsverzeichnis, List<String> broadcastKommunikator) {
        this.name = name;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schutzschildeInProzent = schutzschildeInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.photonentorpedos = photonentorpedos;
        this.reparaturAndroiden = repraturAndroiden;
        this.ladungsverzeichnis = ladungsverzeichnis;
        Raumschiffe.broadcastKommunikator = broadcastKommunikator;
    }

    /**
     * Setter f�r den Parameter name.
     * @param nameRaumschiff Bezeichnung des Raumschiffs
     */
    public void setName(String nameRaumschiff) {
        name = nameRaumschiff;
    }

    /**
     * Getter des Parameters name.
     * @return Bezeichnung des Raumschiffs als String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter f�r die Energieversorgung es Raumschiffs.
     * @param prozent Prozentualer Anteil der noch vorhandenen Energieversorgung
     */
    public void setEnergieversorgungInProzent(int prozent) {
        energieversorgungInProzent = prozent;
    }

    /**
     * Getter f�r die Energieversorgung des Raumschiffs.
     * @return Prozentualer Anteil der noch vorhandenen Energieversorgung als int
     */
    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    /**
     * Setter f�r den Schutzschild des Raumschiffs.
     * @param prozent Prozentualer Anteil des noch vorhandenen Schutzschildes
     */
    public void setSchutzschildeInProzent(int prozent) {
        schutzschildeInProzent = prozent;
    }

    /**
     * Getter f�r den Schutzschild des Raumschiffs.
     * @return Prozentualer Anteil der noch vorhandenen Schutzschildes als int
     */
    public int getSchutzschildeInProzent() {
        return schutzschildeInProzent;
    }

    /**
     * Setter f�r das Lebenserhaltungssystem des Raumschiffs.
     * @param prozent Prozentualer Anteil des noch vorhandenen Lebenserhaltungssystems
     */
    public void setLebenserhaltungssystemeInProzent(int prozent) {
        lebenserhaltungssystemeInProzent = prozent;
    }

    /**
     * Getter f�r das Lebenserhaltungsssystem des Raumschiffs.
     * @return Prozentualer Anteil des noch intakten Lebenserhaltungssystems als int
     */
    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    /**
     * Setter f�r die H�lle des Raumschiffs.
     * @param prozent Prozentualer Anteil der noch intakten H�lle
     */
    public void setHuelleInProzent(int prozent) {
        huelleInProzent = prozent;
    }

    /**
     * Getter f�r die H�lle des Raumschiffs.
     * @return Prozentualer Anteil der noch intakten H�lle als int
     */
    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    /**
     * Setter f�r die Anzahl der Photonentorpedos des Raumschiffs.
     * @param anzahl Anzahl der Photonentorpedos
     */
    public void setPhotonentorpedos(int anzahl) {
        photonentorpedos = anzahl;
    }

    /**
     * Getter f�r die Photonentorpedos des Raumschiffs.
     * @return Anzahl der Photonentorpedos als int
     */
    public int getPhotonentorpedos() {
        return photonentorpedos;
    }

    /**
     * Setter f�r die Anzahl der Reparaturandroiden des Raumschiffs.
     * @param androiden Anzahl der Reparaturandroiden
     */
    public void setReparaturAndroiden(int androiden) {
        reparaturAndroiden = androiden;
    }

    /**
     * Getter f�r die Anzahl der Reparaturandroiden des Raumschiffs.
     * @return Anzahl der Reparaturandroiden als int
     */
    public int getReparaturAndroiden() {
        return reparaturAndroiden;
    }

    /**
     * Setter f�r die im Raumschiff vorhandene Ladung.
     * @param anzahl Liste der Ladung im Raumschiff
     */
    public void setLadungsverzeichnis(Ladung[] anzahl) {
        ladungsverzeichnis = anzahl;
    }

    /**
     * Getter f�r die im Raumschiff vorhandene Ladung.
     * @return Die Ladung im Rausmchiff als String-Liste
     */
    public Ladung[] getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    /**
     * Setter f�r Broadcast-Kommunikator.
     * @param broadcast Nachrichten an alle Raumschiffe.
     */
    public void setBroadcastKommunikator(List<String> broadcast) {
        broadcastKommunikator = broadcast;
    }

    /**
     * Getter f�r Broadcast-Kommunikator, welcher alle Nachrichten an die Raumschiffe enth�lt.
     * @return Nachrichten an alle Raumschiffe als String-Liste.
     */
    public List<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }
    /**
     * Die Methode wird verwendet, um ein gegnerisches mit Photonentorpedos zu beschie�en. Sind keine Torpedos vorhanden, gibt er nur die Nachricht "-=Click=-" aus. Ansonsten wird ein Torpedo von der Anzahl abgezogen und die Methode treffer f�r das andere Raumschiff aufgerufen.
     * @param haeufigkeit Beschreibt wie oft die Methode und somit wie oft das gegnerische Raumschiff abgeschossen werden soll
     * @param nameGegner Ist das Raumschiff, welches abgeschossen werden soll
     *
     */
    public void photonenAbschiessen(int haeufigkeit, Raumschiffe nameGegner) {

        for (int i = 0; i < haeufigkeit; i++) {
            if (photonentorpedos == 0) {
                nachrichtAnAlle("-=Click=-");

            } else {
                photonentorpedos = photonentorpedos - 1;
                nachrichtAnAlle("Photonentorpedo abgeschossen");
                nameGegner.treffer();
            }
        }

    }

    /**
     * Wird die Methode f�r ein Raumschiff aufgerufen so verringern sich, je nachdem wie viel von welchem System noch vorhanden ist, die H�lle, das Schild, die Energieversorgung oder das Lebenserhaltungssystem um 50%.
     *
     */
    public void treffer() {

        setSchutzschildeInProzent(schutzschildeInProzent - 50);
        if (schutzschildeInProzent <= 0) {
            setHuelleInProzent(huelleInProzent - 50);
            setEnergieversorgungInProzent(energieversorgungInProzent - 50);
            if (huelleInProzent <= 0) {
                nachrichtAnAlle("Lebenserhaltungssystem wurden vernichtet.");
            }
        }
    }

    /**
     * Die Methode funktioniert �hnlich zu photonenAbschie�en.
     * @param nameGegner Raumschiff, welches abgeschossen werden soll.
     */
    public void phaserkanonenAbschiessen(Raumschiffe nameGegner) {

        if (energieversorgungInProzent < 50) {
            nachrichtAnAlle("-=Click=-");
        } else {
            setEnergieversorgungInProzent(energieversorgungInProzent - 50);
            nachrichtAnAlle("Phaserkanone abgeschossen");
            nameGegner.treffer();
        }
    }

    /**
     * Die Methode gibt die Nachricht and den f�r alle Raumschiffe einsehbaren broadcastKommunikator weiter.
     * @param nachricht Enth�lt die Nachricht, die an alle Raumschiffe versendet werden soll.
     */
    public void nachrichtAnAlle(String nachricht) {

        broadcastKommunikator.add(nachricht);
    }

    /**
     * Die Methode gibt den broadcastKommunikator und somit alle an ihn gesendeten Nachrichten zur�ck.
     * @return Sie gibt den broadcastKommunikator als String-Liste zur�ck.
     */
    public List<String> logbuchEintraege() {

        return broadcastKommunikator;
    }

    /**
     * Die Methode dient dazu alle Attribute eines Raumschiffes auszugeben.
     */
    public void zustandAusgeben() {

        System.out.printf("\nRaumschiff: %s\nAnzahl Photonentorpedos: %d\nEnergieversorgung in Prozent: %d%%\nSchutzschild in Prozent: %d%%\nH�lle in Prozent: %d%%\nLebenserhaltungssysteme in Prozent: %d%%\nAnzahl der Reparaturandroiden: %d\n\n", name, photonentorpedos, energieversorgungInProzent, schutzschildeInProzent, huelleInProzent, lebenserhaltungssystemeInProzent, reparaturAndroiden);
    }
    /**
     * Die Methode gibt alle im Raumschiff vorhandenen Ladungen aus.
     */
    public void ladungsverzeichnisAusgeben() {

        System.out.printf("Ladungsverzeichnis %s: \n", name);
        for (int i = 0; i < ladungsverzeichnis.length; i++) {
            System.out.println(ladungsverzeichnis[i]);
        }
    }


}
